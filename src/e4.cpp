// Largest palindrome product


//
//A palindromic number reads the same both ways.
//The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
//Find the largest palindrome made from the product of two 3-digit numbers.


#include <iostream>
using namespace std;

int palindrome(long int num) {
    long int n = num;
    long int rev = 0;
    while(num != 0) {
        long int digit = num % 10;
        rev = (rev * 10) + digit;
        num = num / 10;
    }
    if (n == rev) {
        return rev;
    } else {
        return 0;
    }
}

int main () {
    int a = 100;
    long int palin = 0;
    while (a <= 999) {
        int b = 100;
        while (b <= 999) {
            long int s = a * b;
            long int temp = palindrome(s);
            if(temp != 0) {
                if (temp < palin) {
                    break;
                } else {
                    palin = temp;
                }
            }
            b++;
        }
        a++;
    }
    cout << "Result: " << palin << endl;
}

