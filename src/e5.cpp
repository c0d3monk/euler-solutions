
// Smallest multiple

//2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
//
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

#include <iostream>
using namespace std;

int evenly (long int num) {
    for (int i = 11; i <= 20; i++) {
        if (num % i) {
            return 0;
        }
    }
    return num;
}

int main() {
    long int n = 2520;
    while(true) {
        long int r = evenly(n);
        if(r) {
            cout << "Result: " << r;
            break;
        }
        n++;
    }
    return 0;
}

// Faster
//#include <iostream>
//using namespace std;
//int main() {
//    int i; int t=0;
//    for (i=1;;i++) {
//        if ((i%11)==0 && (i%12)==0&&(i%13)==0 && (i%14)==0 && (i%15)==0 && (i%16)==0 && (i%17)==0 && (i%18)==0 && (i%19)==0 && (i%20)==0)
//            break;
//    }
//    cout<<i;
//    return 0;
//}
